THIS REPO IS DISCONTINUED
- Too much spam through our smtp server
- It is simpler and more friendly to juste put your mail address on your website
# Contact Mailer
A minimal python app to send mail when people fills in your contact form!

## Why?

Here at Jean-Cloud, we are fans of static websites. They are easily distributed, use few ressources and are very secured.

But the first limitation is… No interractions… So no contact form… That would be a shame to deploy a website without a contact section!

## How does it work?
### Create the contact form
- Create or reuse an existing contact form
- Add the `action` parameter to the mailer address + `/submit`
- Get a token from this mailer app and place it in your form like :

```
<form action="https://mailer.jean-cloud.net/submit" method="POST">
    <input type="mail" name="mail" />
    <input type="text" name="nom" />
    <input type="text" name="objet" />
    <textarea name="contenu" required="required"></textarea>
    <input type="hidden" name="token" value="eYEqyDXk4bKgFrwuWdN0B9UZtOnIlJcRzx8mh2jCsPTAH7V5QS" />
    <input type="submit" />
</form>
```

### Identifying on the mailer
Ask for an account to the mailer admin.
  They will hand you a password like : `hepaNRSkqcxKD1QujwTr9I6XyiA7VOCGnsWbMtdlZJz0o8ULHP`  

### Configure the mail sending
You can choose how information sent with form will be displayed in the final mail! Actually there are two templatable fields: the mail subject and content.
Here is a template example :
```
subject: "[Contact] {{nom|anonyme}} — {{object|no object}}"
content: "Message de contact :%0D%0A{{message}}"
```
When the form will be submitted to the mailer, parts like `{{…}}` will be replaced by fields content of the same name!  
If the field holds no value, two possibilities:
- The default value specified after `|` is used
- There is no default value, the form is rejected.
TIP: you can set an empty default value in order to have a facultative field: `{{name|}}`.

## Installation
### Docker
`docker pull jeancloud/contact-mailer`
### Git & Pip in virtualenv
```
git clone https://git.jean-cloud.net/adrian/contact-mailer
cd contact-mailer
virtualenv venv
. venv/bin/activate
pip install -r requirements.txt
```

## Running it
The app needs a lot of env vars to run :
```
SMTP_SERVER_ADDRESS=mail.gandi.net
SMTP_SERVER_PORT=465
SMTP_SSL=true
SMTP_SERVER_USERNAME=noreply@example.net
SMTP_SERVER_PASSWORD=bigpass
SMTP_SERVER_SENDER=noreply@example.net
MONGODB_HOST=mongodb
ADMIN_PASSWORD=test
UID=1000
```
You can store them in a `.env` file. The python app will read it or you can pass it to the docker container with `run` option `--env-file`.

- `SMTP_*` are used to connect to the smtp server that will send the mail.
- `SMTP_SSL` is used to enable SSL
- `SMTP_STARTTLS` is used to enable STARTTLS if `SMTP_SSL` is not defined. In no ssl and starttls are defined the app wont connect.
- `MONGODB_HOST` the host to connect to mongodb
- `ADMIN_PASSWORD` password used to manage users
- `UID` used to set the uwsg socket ownership in production

## Client

plain or light theme.

## Roadmap
### Near future
- use a standart logger (used by bottle and uwsgi) to log error on mail fail
- [unit tests](https://bottlepy.org/docs/dev/recipes.html#unit-testing-bottle-applications)
- add redirection urls to form config
- Include some [capcha](https://alternativeto.net/software/recaptcha/) support
- Correctly escape html entities
- Sign mails with the server key
- Use a dedicated SMTP server

### Ameliorations
- Use real user/passwords accounts
- Create a gui client
