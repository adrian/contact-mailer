set -e
version=2.1.0
docker build -t jeancloud/contact-mailer:latest -t jeancloud/contact-mailer:$version .
docker push jeancloud/contact-mailer:latest
docker push jeancloud/contact-mailer:$version
