/* Executed after page loading */
(function () {

class JeanCloudContactFormNotifier {
    constructor (theme, messageContainer) {
        /* Choose the theme */
        this.theme = theme ? theme : 'light'

        /* Create or get the message container */
        if (messageContainer) {
            this.messageContainer = messageContainer
        } else {
            this.messageContainer = document.createElement('div')
            this.messageContainer.classList.add('contact-mailer-message-container')
            document.body.appendChild(this.messageContainer)
        }
    }

    message(level, text) {
        /* This function displays a message */
        const messageElement = document.createElement('p')
        const messageId = 'contact-mailer-' + this.theme + '-message-' + level
        this.messageContainer.appendChild(messageElement)
        messageElement.textContent = text
        messageElement.classList.add('contact-mailer-message')
        messageElement.classList.add(messageId)
        messageElement.id = messageId

        /*add close button to the alert message*/
        const closeButtonElement = document.createElement('span')
        messageElement.appendChild(closeButtonElement)
        closeButtonElement.textContent = "×"
        closeButtonElement.classList.add('contact-mailer-message-close-button')
        closeButtonElement.title = 'Cliquer pour fermer'
        closeButtonElement.onclick = () => {
            messageElement.parentNode.removeChild(messageElement)
        }

        //setTimeout(() => {
        //    try {
        //        messageElement.parentNode.removeChild(messageElement)
        //    } catch (e) {} /* Silently fail if message was already deleted by click */
        //}, 10000)

    }

    success (text) {
        this.message('success', text)
    }

    error (text) {
        this.message('error', text)
    }
}


function jeanCloudContactFormIntercept (formId, notifier) {
    /*
     * This function intercepts a form submission and send it via XHR.
     * Param formId is the HTML id of the form
     */
    const formElem = document.getElementById(formId)
    if (!formElem) {
        console.error('You tried to intercept form with id:"' + formId + '" but it was not found.')
        return
    }

    if (!notifier)
        console.log('No notifier given, no message can be displayed')

    /* Intercept the submit event */
    formElem.onsubmit = async (e) => {
        e.preventDefault()
        /* Add loading text */
        const submitButton = formElem.querySelector('[type="submit"]')
        const loadingText = document.createElement('span')
        loadingText.classList.add("contact-mailer-sending");
        loadingText.textContent = 'Envoi en cours…'
        submitButton.after(loadingText)

        /* Add the filling timer in seconds */
        const timerField = document.createElement('input')
        timerField.value = Math.round((Date.now() - contactMailerPageLoadedTime) / 1000)
        timerField.name = 'timerfield'
        timerField.hidden = 'hidden'
        formElem.appendChild(timerField)

        /* XHR */
        fetch(formElem.action, {
            method: formElem.method,
            body: new FormData(formElem)
        })

        .then(data => {
            loadingText.parentNode.removeChild(loadingText)
            if (data.ok && data.status == 200) {
                notifier.success('Le message a bien été envoyé !')
                formElem.reset()
            } else if (!data.ok && data.status == 500) {
                notifier.error('Erreur du service d’envoi. Réessayez plus tard ou contactez https://jean-cloud.net')
            } else if (!data.ok && data.status == 400) {
                notifier.error('Une erreur est survenue dans la requête que vous avez effectué. Réessayez plus tard ou contactez le webmaster par un autre moyen.')
                // TODO display server’s error message
            }
        })

        .catch((error) => {
            console.error(error)
            loadingText.parentNode.removeChild(loadingText)
            notifier.error('Impossible d’envoyer le formulaire. Vérifiez votre connexion internet ou réessayez plus tard.')
        })

        /* Remove timer field after xhr. So we can try again. */
        formElem.removeChild(timerField)
    }
}

    /* Get the current js file location */
    const path = (document.currentScript.src[-1] == '/' ? document.currentScript.src : document.currentScript.src.replace(/\/[^\/]*$/, ''))
    
    /* Adding a css file */
    var link = document.createElement("link");
    link.type        = "text/css";
    link.rel         = "stylesheet";
    link.crossOrigin = 'anonymous';
    link.href        = path + "/style.css";
    link.integrity   = 'sha384-D12RSMaIURTgZZljhdQqYlQzgEfXvOFwtiqzkWnNcDbKFwMWXcmsCRFO5BNii0MB'
    // cat style.css | openssl dgst -sha384 -binary | openssl base64 -A
    document.head.appendChild(link);

    /* Load the targeted forms */
    var configs = document.getElementsByClassName('contact-form-config')
    for (var i=0; i<configs.length; i++) {
        var formId = configs[i].getAttribute('form-id')
        var theme  = configs[i].getAttribute('notify-theme')
        jeanCloudContactFormIntercept(formId, new JeanCloudContactFormNotifier(theme))
    }

    var contactMailerPageLoadedTime = Date.now()
})()

