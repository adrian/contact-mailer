var app = new Vue({
  el: '#app',
  data: {
      type: 'admin_pass', /* admin_pass or token */
      password: 'test',
      loggedin: false,
      mailerHost: 'https://mailer.jean-cloud.net',
      //mailerHost: 'http://localhost:8080',
      //mailerHost: '/api',
      forms: [],
      users: [],
      newUser: '',
      page:'new_user',
      newForm: {
          'content': '{{message}}',
          'subject': '[contact jean-cloud.net] {{nom|annonyme}} — {{objet}}',
          'mail': 'contact@jean-cloud.org',
          'honeypotfield': 'prenom',
          'timerdelay': 5,
      }
  },
  methods: {
      login: function () {
          if (!this.type) {
              console.log('missing type')
              return
          }
          if (!this.password) {
              console.log('missing password')
              return
          }
          this.loggedin = true
          this.getForms()
          if ( this.type == 'admin_pass' )
            this.getUsers()
      },
      logout: function () {
        this.type = 'token'
        this.password = null
        this.loggedin = false
      },
      getForms: function () {
        fetch(this.mailerHost + '/form/list', {
            method: 'POST',
            body: this.type + '=' + this.password
        })
        .then(response => response.json())
        .then(data => {
            this.forms = data.data
        })
        .catch((error) => {
            console.error(error)
        })
      },
      getUsers: function () {
        fetch(this.mailerHost + '/user/list', {
            method: 'POST',
            body: this.type + '=' + this.password
        })
        .then(response => response.json())
        .then(data => {
            this.users = data.data
        })
        .catch((error) => {
            console.error(error)
        })
      },
      addForm: function () {
          fetch(this.mailerHost + '/form', {
              method: 'post',
              body: this.type + '=' + this.password
                + '&subject=' + this.newForm.subject
                + '&mail=' + this.newForm.mail
                + '&content=' + this.newForm.content
                + '&honeypotfield=' + this.newForm.honeypotfield
                + '&timerdelay=' + this.newForm.timerdelay
          })
          .then(data => {
              console.log(data)
              this.getForms()
          })
          .catch(error => {
              console.log(error)
          })
      },
      addUser: function () {
          if (!this.newUser) {
              console.log('need username')
              return
          }
          fetch(this.mailerHost + '/user/' + this.newUser, {
              method: 'put',
              body: this.type + '=' + this.password
          })
          .then(data => {
              this.newUser = ''
              this.getUsers()
          })
          .catch((error) => {
              console.error(error)
          })

      },
      deleteForm: function (formId) {
          fetch(this.mailerHost + '/form/' + formId, {
              method: 'delete',
              body: this.type + '=' + this.password
          })
          .then(data => {
              console.log(data)
              this.getForms()
          })
          .catch((error) => {
              console.error(error)
          })
      },
  },
})
